load :- !,
    writeln(">> Loading utils..."),
    consult(utils),
    writeln(">> Loading simplify..."),
    consult(simplify),
    writeln(">> Loading dnf..."),
    consult(dnf),

    writeln(">> Loading additional libraries..."),
    use_module(library(dialect/ifprolog)),
    use_module(library(ordsets)),
    use_module(library(pcre)),

    writeln("Done.").


cmd_simplify :-
    writeln("Enter the formula you'd like to simplify."),
    read_string(user, "\n", "\r\t ", _, Input),
    parse_formula(Input, Formula)
    -> join(["Simplified formula: ", Formula], Output), writeln(Output)
    ; writeln("Couldn't parse the formula :(").


cmd_equiv :-
    writeln("Enter the formula you'd like to analyze first."),
    read_string(user, "\n", "\r\t ", _, LhsInput),
    parse_formula(LhsInput, Lhs)
    -> (
        writeln("Enter another formula to compare with."),
        read_string(user, "\n", "\r\t ", _, RhsInput),
        parse_formula(RhsInput, Rhs)
        -> (
            join([
                "Simplified formula #1: ",
                Lhs,
                "\nSimplified formula #2: ",
                Rhs
            ], Output),
            writeln(Output),
            Lhs == Rhs
            -> writeln("Formulae are equivalent!")
            ; writeln("Formulae are NOT equivalent!")
        )
        ; writeln("Couldn't parse formula for the right-hand side operand :(")
    )
    ; writeln("Couldn't parse formula for the left-hand side operand :(").


process_command("simplify") :-
    once(cmd_simplify).
process_command("equiv") :-
    once(cmd_equiv).
process_command("help") :-
    join_with([
        "Welcome to this AMAZING utility for operating with boolean formulae!",
        "You are free to use any of the commands below.",
        "",
        "- help/info: show this message",
        "- simplify: simplify the given boolean formula (you don't wanna know what's going on under the hood!)",
        "- equiv: check if two boolean formulae are equivalent or not",
        "- quit/exit: run away from this perfect beautiful program :("
    ], "\n", Help),
    writeln(Help).
process_command("info") :-
    process_command("help").
process_command("exit") :-
    writeln("Bye! Hope to see you soon :)"),
    halt.
process_command("quit") :-
    process_command("exit").
process_command(Command) :-
    join([
        "Unknown command: '", 
        Command,
        "'.\n",
        "Try running 'help' (or 'info') to see the list of available commands."
    ], Error),
    writeln(Error).


main_cycle :- !,
    nl,
    write(">>> "),
    read_string(user, "\n", "\r\t ", _, Command),
    once(process_command(Command)).


main :-
    load,
    repeat,
    main_cycle,
    fail.
