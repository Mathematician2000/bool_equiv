simplify_ast(AST, SimplifiedAST) :-
    kick_equiv_xor(AST, AST_1),
    kick_implies(AST_1, AST_2),
    push_negations(AST_2, AST_3),
    distr_conj(AST_3, AST_4),
    make_absorbs(AST_4, AST_5),
    fold_constants(AST_5, SimplifiedAST),
    !.


% kick_equiv_xor:
% A ~ B = A & B v -A & -B
% A + B = -A & B v A & -B

kick_equiv_xor(const(X),
               const(X)) :- !.
kick_equiv_xor(variable(X),
               variable(X)) :- !.

kick_equiv_xor(ast(operator("-"), X),
               ast(operator("-"), X1)) :- !,
    kick_equiv_xor(X, X1).

kick_equiv_xor(ast(operator("~"), X, Y),
               ast(operator("v"), ast(operator("&"), X1, Y1), ast(operator("&"), ast(operator("-"), X1), ast(operator("-"), Y1)))) :- !,
    kick_equiv_xor(X, X1),
    kick_equiv_xor(Y, Y1).
kick_equiv_xor(ast(operator("+"), X, Y),
               ast(operator("v"), ast(operator("&"), ast(operator("-"), X1), Y1), ast(operator("&"), X1, ast(operator("-"), Y1)))) :- !,
    kick_equiv_xor(X, X1),
    kick_equiv_xor(Y, Y1).

kick_equiv_xor(ast(operator(Op), X, Y),
               ast(operator(Op), X1, Y1)) :- !,
    kick_equiv_xor(X, X1),
    kick_equiv_xor(Y, Y1).

kick_equiv_xor(AST, _) :- !,
    term_string(AST, AstStr),
    join(["Failed to simplify AST [kick_equiv_xor]: cannot parse subtree '", AstStr, "'."], Error),
    writeln(Error),
    fail.


% kick_implies:
% A > true = true
% A > A = true
% A > B = -A v B

kick_implies(const(X),
             const(X)) :- !.
kick_implies(variable(X),
             variable(X)) :- !.

kick_implies(ast(operator("-"), X),
             ast(operator("-"), X1)) :- !,
    kick_implies(X, X1).

kick_implies(ast(operator(">"), _, const("true")),
             cosnt("true")) :- !.
kick_implies(ast(operator(">"), X, X),
             cosnt("true")) :- !.

kick_implies(ast(operator(">"), X, Y),
             ast(operator("v"), ast(operator("-"), X1), Y1)) :- !,
    kick_implies(X, X1),
    kick_implies(Y, Y1).

kick_implies(ast(operator(Op), X, Y),
             ast(operator(Op), X1, Y1)) :- !,
    kick_implies(X, X1),
    kick_implies(Y, Y1).

kick_implies(AST, _) :- !,
    term_string(AST, AstStr),
    join(["Failed to simplify AST [kick_implies]: cannot parse subtree '", AstStr, "'."], Error),
    writeln(Error),
    fail.


% push_negations:
% --A = A
% -(A v B) = -A & -B
% -(A & B) = -A v -B

push_negations(const(X),
               const(X)) :- !.
push_negations(ast(operator("-"), const("true")),
               const("false")) :- !.
push_negations(ast(operator("-"), const("false")),
               const("true")) :- !.

push_negations(variable(X),
               variable(X)) :- !.
push_negations(ast(operator("-"), variable(X)),
               ast(operator("-"), variable(X))) :- !.

push_negations(ast(operator(Op), X, Y),
               ast(operator(Op), X1, Y1)) :- !,
    push_negations(X, X1),
    push_negations(Y, Y1).

push_negations(ast(operator("-"), ast(operator("-"), X)),
               X1) :- !,
    push_negations(X, X1).
push_negations(ast(operator("-"), ast(operator("v"), X, Y)),
               ast(operator("&"), X1, Y1)) :- !,
    push_negations(ast(operator("-"), X), X1),
    push_negations(ast(operator("-"), Y), Y1).
push_negations(ast(operator("-"), ast(operator("&"), X, Y)),
               ast(operator("v"), X1, Y1)) :- !,
    push_negations(ast(operator("-"), X), X1),
    push_negations(ast(operator("-"), Y), Y1).

push_negations(AST, _) :- !,
    term_string(AST, AstStr),
    join(["Failed to simplify AST [push_negations]: cannot parse subtree '", AstStr, "'."], Error),
    writeln(Error),
    fail.


% distr_conj:
% A & (B v C) = A & B v A & C
% (A v B) & C = A & C v B & C

distr_conj(const(X),
           const(X)) :- !.
distr_conj(variable(X),
           variable(X)) :- !.

distr_conj(ast(operator("&"), X, ast(operator("v"), Y, Z)),
           ast(operator("v"), Lhs, Rhs)) :- !,
    distr_conj(X, X1),
    distr_conj(Y, Y1),
    distr_conj(Z, Z1),
    distr_conj(ast(operator("&"), X1, Y1), Lhs),
    distr_conj(ast(operator("&"), X1, Z1), Rhs).
distr_conj(ast(operator("&"), ast(operator("v"), X, Y), Z),
           AST) :- !,
    distr_conj(ast(operator("&"), Z, ast(operator("v"), X, Y)), AST).

distr_conj(ast(operator("-"), X),
           ast(operator("-"), X1)) :- !,
    distr_conj(X, X1).
distr_conj(ast(operator(Op), X, Y),
           Result) :- !,
    distr_conj(X, X1),
    distr_conj(Y, Y1),
    (
        X = X1, Y = Y1
        -> Result = ast(operator(Op), X1, Y1)
        ; distr_conj(ast(operator(Op), X1, Y1), Result)
    ).

distr_conj(AST, _) :- !,
    term_string(AST, AstStr),
    join(["Failed to simplify AST [distr_conj]: cannot parse subtree '", AstStr, "'."], Error),
    writeln(Error),
    fail.


% make_absorbs:
% A v A = A & A = A
% A v -A = true
% A & -A = false
% (A & B) v (-A & B) = B
% A & (-A v B) = A v B = A v (-A & B)
% A & (A v B) = A = A v (A & B)

make_absorbs(AST, DNF) :-
    get_conjs(AST, Conjs),
    total_absorb_conjs(Conjs, CleanConjs),
    build_dnf(CleanConjs, DNF),
    !.


total_absorb_conjs(Conjs, Result) :-
    absorb_conjs(Conjs, [], NewConjs),
    (
        ord_seteq(Conjs, NewConjs)
        -> Result = Conjs
        ; total_absorb_conjs(NewConjs, Result)
    ).


absorbing_insert([P1, N1], Conjs, Conjs) :-
    member([P2, N2], Conjs),
    ord_subset(P2, P1),
    ord_subset(N2, N1),
    !.
absorbing_insert(Pair, Conjs, NewConjs) :-
    remove_supersets(Pair, Conjs, CleanConjs),
    ord_add_element(CleanConjs, Pair, NewConjs).


absorb_conjs([], Conjs, Conjs) :- !.

absorb_conjs([[P1, N1] | Tail], Conjs, Result) :-
    member(X, P1),
    member([P2, N2], Conjs),
    ord_memberchk(X, N2),
    ord_del_element(P1, X, P),
    ord_subset(P, P2),
    ord_del_element(N2, X, N),
    ord_subset(N1, N),
    !,
    absorbing_insert([P1, N1], Conjs, Conjs_1),
    absorbing_insert([P2, N], Conjs_1, Conjs_2),
    absorb_conjs(Tail, Conjs_2, Result).
absorb_conjs([[P1, N1] | Tail], Conjs, Result) :-
    member(X, P1),
    member([P2, N2], Conjs),
    ord_memberchk(X, N2),
    ord_del_element(P1, X, P),
    ord_subset(P2, P),
    ord_del_element(N2, X, N),
    ord_subset(N, N1),
    !,
    absorbing_insert([P, N1], Conjs, Conjs_1),
    absorb_conjs(Tail, Conjs_1, Result).

absorb_conjs([[P1, N1] | Tail], Conjs, Result) :-
    member(X, N1),
    member([P2, N2], Conjs),
    ord_memberchk(X, P2),
    ord_del_element(N1, X, N),
    ord_subset(N, N2),
    ord_del_element(P2, X, P),
    ord_subset(P1, P),
    !,
    absorbing_insert([P1, N1], Conjs, Conjs_1),
    absorbing_insert([P, N2], Conjs_1, Conjs_2),
    absorb_conjs(Tail, Conjs_2, Result).
absorb_conjs([[P1, N1] | Tail], Conjs, Result) :-
    member(X, N1),
    member([P2, N2], Conjs),
    ord_memberchk(X, P2),
    ord_del_element(N1, X, N),
    ord_subset(N2, N),
    ord_del_element(P2, X, P),
    ord_subset(P, P1),
    !,
    absorbing_insert([P1, N], Conjs, Conjs_1),
    absorb_conjs(Tail, Conjs_1, Result).

absorb_conjs([Pair | Tail], Conjs, Result) :- !,
    absorbing_insert(Pair, Conjs, NewConjs),
    absorb_conjs(Tail, NewConjs, Result).


remove_supersets([_, _], [], []) :- !.
remove_supersets([P1, N1], [[P2, N2] | Tail], Result) :-
    ord_subset(P1, P2),
    ord_subset(N1, N2),
    !,
    remove_supersets([P1, N1], Tail, Result).
remove_supersets(Pair, [Elem | Tail], [Elem | Result]) :- !,
    remove_supersets(Pair, Tail, Result).


build_dnf([], _) :- !,
    writeln("Failed to simplify AST [make_absorbs]: got empty tree."),
    fail.
build_dnf([[[], []]], const("true")) :- !.
build_dnf([[P, N]], Conj) :- !,
    build_conj(P, N, Conj).
build_dnf([[P, N] | Tail], ast(operator("v"), Conj, DNF)) :- !,
    build_conj(P, N, Conj),
    build_dnf(Tail, DNF).


build_conj(["false" | _], _, const("false")) :- !.
build_conj(["true" | T], Y, Conj) :- !,
    build_conj(T, Y, Conj).
build_conj([X | T], Y, ast(operator("&"), variable(X), Conj)) :- !,
    build_conj(T, Y, Conj).
build_conj([], [Y | T], ast(operator("&"), ast(operator("-"), variable(Y)), Conj)) :- !,
    build_conj([], T, Conj).
build_conj([], [], const("true")) :- !.


get_conjs(const(X), [[[X], []]]) :- !.
get_conjs(variable(X), [[[X], []]]) :- !.
get_conjs(ast(operator("-"), const(X)), [[[], [X]]]) :- !.
get_conjs(ast(operator("-"), variable(X)), [[[], [X]]]) :- !.
get_conjs(ast(operator("v"), X, Y), SetConjs) :- !,
    get_conjs(X, SetX),
    get_conjs(Y, SetY),
    ord_union(SetX, SetY, SetConjs).
get_conjs(ast(operator("&"), X, Y), [ConsistentSet]) :- !,
    get_conj_vars(ast(operator("&"), X, Y), SetPos, SetNeg),
    make_consistent(SetPos, SetNeg, ConsistentSet).


get_conj_vars(const(X), [X], []) :- !.
get_conj_vars(variable(X), [X], []) :- !.

get_conj_vars(ast(operator("-"), const(X)), [], [X]) :- !.
get_conj_vars(ast(operator("-"), variable(X)), [], [X]) :- !.

get_conj_vars(ast(operator("&"), X, Y), SetPos, SetNeg) :- !,
    get_conj_vars(X, XPos, XNeg),
    get_conj_vars(Y, YPos, YNeg),
    ord_union(XPos, YPos, SetPos),
    ord_union(XNeg, YNeg, SetNeg).


make_consistent(SetPos, SetNeg, [["false"], []]) :-
    ( member("false", SetPos)
    ; member("true", SetNeg)
    ; ord_intersect(SetPos, SetNeg, [_ | _])
    ), !.
make_consistent(SetPos, SetNeg, [CleanSetPos, CleanSetNeg]) :- !,
    ord_del_element(SetPos, "true", CleanSetPos),
    ord_del_element(SetNeg, "false", CleanSetNeg).


% fold_constants:
% -true = false
% -false = true
% true & A = A & true = A
% false & A = A & false = false
% true v A = A v true = true
% false v A = A v false = A

fold_constants(const(X),
               const(X)) :- !.
fold_constants(variable(X),
               variable(X)) :- !.

fold_constants(ast(operator("-"), const("true")),
               const("false")) :- !.
fold_constants(ast(operator("-"), const("false")),
               const("true")) :- !.

fold_constants(ast(operator("-"), X),
               AST) :- !,
    fold_constants(X, X1),
    (
        X1 = const(_)
        -> fold_constants(ast(operator("-"), X1), AST)
        ; AST = ast(operator("-"), X1)
    ).

fold_constants(ast(operator("&"), const("false"), _),
               const("false")) :- !.
fold_constants(ast(operator("&"), const("true"), X),
               X1) :- !,
    fold_constants(X, X1).

fold_constants(ast(operator("v"), const("true"), _),
               const("true")) :- !.
fold_constants(ast(operator("v"), const("false"), X),
               X1) :- !,
    fold_constants(X, X1).

fold_constants(ast(operator(Op), X, const(Y)),
               AST) :- !,
    fold_constants(ast(operator(Op), const(Y), X), AST).

fold_constants(ast(operator(Op), X, Y),
               AST) :- !,
    fold_constants(X, X1),
    fold_constants(Y, Y1),
    (
        ( X1 = const(_) ; Y1 = const(_) )
        -> fold_constants(ast(operator(Op), X1, Y1), AST)
        ; AST = ast(operator(Op), X1, Y1)
    ).

fold_constants(AST, _) :- !,
    term_string(AST, AstStr),
    join(["Failed to simplify AST [fold_constants]: cannot parse subtree '", AstStr, "'."], Error),
    writeln(Error),
    fail.
