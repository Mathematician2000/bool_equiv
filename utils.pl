% List operations

enumerate([], []).
enumerate([X], [[0, X]]) :- !.
enumerate([X | Tail], [[N1, X] | Pairs]) :-
    enumerate(Tail, Pairs),
    Pairs = [[N, _] | _],
    N1 is N + 1.


every_second([], []) .
every_second([X], [X]) .
every_second([X, _ | Rest], [X | Tail]) :-
    every_second(Rest, Tail).


% String operations

head(String, Letter) :-
    sub_string(String, 0, 1, _, Letter).


join(Strings, Result) :- join_with(Strings, "", Result).


join_with([], _, "").
join_with([String], _, String) :- !.
join_with([String | Rest], With, Result) :-
    join_with(Rest, With, Suffix),
    string_concat(String, With, Prefix),
    string_concat(Prefix, Suffix, Result).


replace_all(Pattern, With, String, Result) :-
    re_split(Pattern, String, Parts),
    every_second(Parts, Left),
    join_with(Left, With, Result).


delete_all(Pattern, String, Result) :-
    replace_all(Pattern, "", String, Result).
