parse_formula(Input, Formula) :-
    delete_all("\\s+", Input, DryInput),
    tokenize(DryInput, Tokens),
    tokens_to_rpn(Tokens, RPN),
    rpn_to_ast(RPN, AST),
    simplify_ast(AST, SimplifiedAST),
    ast_string(SimplifiedAST, Formula),
    !.


tokenize("", []) :- !.
tokenize(Input, [const(Token) | Tokens]) :-
    member(Token, ["true", "false"]),
    string_concat(Token, Rest, Input),
    !,
    tokenize(Rest, Tokens).
tokenize(Input, [operator(Token) | Tokens]) :-
    member(Token, ["(", ")", "v", "&", ">", "~", "+", "-"]),
    string_concat(Token, Rest, Input),
    !,
    tokenize(Rest, Tokens).
tokenize(Input, [variable(Token) | Tokens]) :-
    string_code(1, Input, Code),
    letter(Code),
    head(Input, Token),
    string_upper(Token, Token),
    string_concat(Token, Rest, Input),
    !,
    tokenize(Rest, Tokens).
tokenize(Input, _) :- !,
    head(Input, Token),
    join(["Tokenization failed: unknown token '", Token, "'."], Error),
    writeln(Error),
    fail.


tokens_to_rpn(Tokens, RPN) :-
    sorting_station(Tokens, [], RPN),
    !.


get_priority("-", 4).
get_priority("&", 3).
get_priority("v", 2).
get_priority(">", 1).
get_priority("~", 0).
get_priority("+", 0).


is_left_assoc("&").
is_left_assoc("v").
is_left_assoc("~").
is_left_assoc("+").

is_right_assoc("-").
is_right_assoc("&").
is_right_assoc("v").
is_right_assoc(">").
is_right_assoc("~").
is_right_assoc("+").


is_unary("-").

is_binary("&").
is_binary("v").
is_binary(">").
is_binary("~").
is_binary("+").


sorting_station([], [], []) :- !.
sorting_station([], [operator("(") | _], _) :- !,
    writeln("Failed to build RPN: missing a closing bracket."),
    fail.
sorting_station([], [operator(X) | Stack], [operator(X) | Output]) :- !,
    sorting_station([], Stack, Output).

sorting_station([const(Token) | Tokens], Stack, [const(Token) | Output]) :- !,
    sorting_station(Tokens, Stack, Output).
sorting_station([variable(Token) | Tokens], Stack, [variable(Token) | Output]) :- !,
    sorting_station(Tokens, Stack, Output).

sorting_station([operator("(") | Tokens], Stack, Output) :- !,
    sorting_station(Tokens, [operator("(") | Stack], Output).

sorting_station([operator(")") | _], [], _) :- !,
    writeln("Failed to build RPN: missing an opening bracket."),
    fail.
sorting_station([operator(")") | Tokens], [operator("(") | Stack], Output) :- !,
    sorting_station(Tokens, Stack, Output).
sorting_station([operator(")") | Tokens], [operator(Op) | Stack], [operator(Op) | Output]) :- !,
    sorting_station([operator(")") | Tokens], Stack, Output).

sorting_station([operator(Op1) | Tokens], [operator(Op2) | Stack], [operator(Op2) | Output]) :-
    get_priority(Op1, P1),
    get_priority(Op2, P2),
    ( P1 < P2 ; P1 == P2, is_left_assoc(Op1) ),
    !,
    sorting_station([operator(Op1) | Tokens], Stack, Output).
sorting_station([operator(Op1) | Tokens], Stack, Output) :- !,
    sorting_station(Tokens, [operator(Op1) | Stack], Output).

sorting_station([Token | _], _, _) :- !,
    term_string(Token, TokenStr),
    join(["Failed to build RPN: unknown token '", TokenStr, "'."], Error),
    writeln(Error),
    fail.


rpn_to_ast(RPN, AST) :-
    rpn_to_ast(RPN, [], AST),
    !.


rpn_to_ast([], [AST], AST) :- !.
rpn_to_ast([], _, _) :- !,
    writeln("Failed to build AST: RPN is incorrect."),
    fail.

rpn_to_ast([const(Token) | Tokens], Stack, AST) :- !,
    rpn_to_ast(Tokens, [const(Token) | Stack], AST).
rpn_to_ast([variable(Token) | Tokens], Stack, AST) :- !,
    rpn_to_ast(Tokens, [variable(Token) | Stack], AST).

rpn_to_ast([operator(Op) | Tokens], Stack, AST) :-
    is_unary(Op),
    Stack = [X | Rest],
    !,
    rpn_to_ast(Tokens, [ast(operator(Op), X) | Rest], AST).
rpn_to_ast([operator(Op) | Tokens], Stack, AST) :-
    is_binary(Op),
    Stack = [Y, X | Rest],
    !,
    rpn_to_ast(Tokens, [ast(operator(Op), X, Y) | Rest], AST).

rpn_to_ast([Token | _], _, _) :- !,
    term_string(Token, TokenStr),
    join(["Failed to build AST: unknown token '", TokenStr, "'."], Error),
    writeln(Error),
    fail.


ast_string(AST, AstStr) :-
    ast_string(AST, AstStr, _).


ast_string(const(X), X, 100) :- !.
ast_string(variable(X), X, 100) :- !.

ast_string(ast(operator(Op), X), AstStr, Priority) :- !,
    ast_string(X, XStr, PrX),
    get_priority(Op, PrOp),
    (
        PrX < PrOp
        -> Priority = PrX, join([Op, "(", XStr, ")"], AstStr)
        ; Priority = PrOp, join([Op, XStr], AstStr)
    ).
ast_string(ast(operator(Op), X, Y), AstStr, Priority) :- !,
    ast_string(X, XStr, PrX),
    ast_string(Y, YStr, PrY),
    get_priority(Op, PrOp),
    (
        PrX < PrOp
        -> join(["(", XStr, ")"], LhsStr)
        ; LhsStr = XStr
    ),
    (
        PrY < PrOp
        -> join(["(", YStr, ")"], RhsStr)
        ; RhsStr = YStr
    ),
    Priority is min(min(PrX, PrY), PrOp),
    join_with([LhsStr, Op, RhsStr], " ", AstStr).

ast_string(AST, _, _) :- !,
    term_string(AST, AstStr),
    join(["Failed to convert AST to string: cannot parse subtree '", AstStr, "'."], Error),
    writeln(Error),
    fail.
