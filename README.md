# bool_equiv

## A small CLI application for operating with boolean formulae

The application works in REPL.

Try running `help` to get the list of available commands.

Main features:
- all the basic operations: inversion (-), conjunction (&), disjunction (v),
material condition (>), equivalence (~), and exclusive OR (+)
- `true` and `false` constants right in the formula!
- up to 26 variables denoted as capital latin letters (from 'A' to 'Z')
- clear and interpretable error messages

Some simple tests are listed in the `examples.txt`.

Have fun! :)
